# relay_bw

Minimal bandwidth measurer for a relay.

It works in a similar way to
[sbws](https://tpo.pages.torproject.net/network-health/sbws/), creating 2 hops
circuits and downloading/uploading data, but it only measures the relay
provided as an argument, not the whole Tor network.

It also doesn't perform
[aggregation nor scaling](https://tpo.pages.torproject.net/network-health/sbws/generator.html)
since that requires having bandwidth measurements for all the relays.

I doesn't generates any
[Bandwidth File](https://spec.torproject.org/bandwidth-file-spec/).

It is intended to be used by operators that would like to check the bandwidth
measurements that sbws calculates, which should be similar to the relay's
`bw_mean` KeyValues in the published
[collector bandwidth files](https://collector.torproject.org/recent/relay-descriptors/bandwidths/).

## Installation

Download this repository:

```bash
git clone https://gitlab.torproject.org/juga/relaybw
cd relay_bw
```

Install [tor](https://gitlab.torproject.org/tpo/core/tor) and
[poetry][] in your system and
activate it.

In a Debian system:

```bash
sudo apt install tor poetry
poetry shell
```

Then install the required libraries:

```bash
poetry install
```

[poetry]: https://python-poetry.org/

## Run it

```bash
relaybw -h
```

It'll print the help with the arguments you should pass to it.

Author: juga at torproject.org

Copyright (c) 2023, The Tor Project, Inc.

Released under the BSD-3-Clause, which applies to all the source code and
binary files except otherwise stated.
