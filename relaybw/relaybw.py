# /usr/bin/env python
import argparse
import logging
import logging.config
import os
import secrets
import sys
from typing import Optional, Tuple

import requests
from stem import Flag, process  # type: ignore
from stem.control import Controller, EventType, Listener  # type: ignore
from stem.descriptor.router_status_entry import RouterStatusEntryV3  # type: ignore
from stem.response.events import StreamEvent  # type: ignore

from webclient import default_logging, defaults, webclient

logging.config.dictConfig(default_logging.LOGGING)
logger = logging.getLogger(__name__)
logger.setLevel(os.environ.get("LOG_LEVEL", "INFO"))


HOME = "/tmp"
APP_DATA_PATH = os.path.join(HOME, ".sbws")
TOR_DATA_PATH = os.path.join(APP_DATA_PATH, "tor")
TOR_CONFIG_BASE = {
    "CookieAuthentication": "1",
    "UseMicrodescriptors": "0",
    "FetchDirInfoEarly": "1",
    "FetchDirInfoExtraEarly": "1",
    "FetchUselessDescriptors": "1",
    "LearnCircuitBuildTimeout": "0",
    # "KISTSchedRunInterval": "1 msec",
    "SocksPort": "9015",
    "ControlPort": "8015",
}
TOR_CONFIG_DIRS = {
    "DataDirectory": TOR_DATA_PATH,
    "PidFile": os.path.join(TOR_DATA_PATH, "pid"),
    "Log": [
        "NOTICE file {}".format(os.path.join(TOR_DATA_PATH, "notices.log"))
    ],
}
TOR_CONFIG = {**TOR_CONFIG_BASE, **TOR_CONFIG_DIRS}
TOR_CONFIG_CAN_FAIL = {
    "ConnectionPadding": "0",
    "CircuitPadding": "0",
    "DormantCanceledByStartup": "1",
}
TOR_CONFIG_RUNTIME = {
    "__DisablePredictedCircuits": "1",
    "__LeaveStreamsUnattached": "1",
}


def launch_tor() -> Tuple[Controller, dict]:
    tor_config: dict = TOR_CONFIG
    os.makedirs(tor_config["DataDirectory"], mode=0o700, exist_ok=True)
    logger.debug("Launching tor with config %s", tor_config)
    process.launch_tor_with_config(tor_config, take_ownership=True)
    logger.info("Launched tor.")
    port: str = tor_config.get("ControlPort", None)
    controller = Controller.from_port(port=int(port))
    logger.debug("Obtained tor controller.")
    controller.authenticate()
    logger.debug("Setting tor options that can fail.")
    for key, value in TOR_CONFIG_CAN_FAIL.items():
        try:
            controller.set_conf(key, value)
        except Exception as e:
            logger.debug("Failed to set option %s", e)
    controller.set_options(TOR_CONFIG_RUNTIME)
    socks_address: Tuple[str, int] = controller.get_listeners(Listener.SOCKS)[
        0
    ]
    proxies: dict = {
        "http": "socks5h://{}:{}".format(*socks_address),
        "https": "socks5h://{}:{}".format(*socks_address),
    }
    return controller, proxies


def attach_stream_to_circuit(
    controller: Controller,
    circuit_id: int,
    session: requests.Session,
    url: str,
) -> int:
    def attach_stream(stream: StreamEvent) -> None:
        if stream.status == "NEW":
            logger.debug(
                "Attaching stream %s to circuit %s", stream.id, circuit_id
            )
            controller.attach_stream(stream.id, circuit_id)

    logger.debug("Adding listener to attach stream to circuit %s", circuit_id)
    controller.add_event_listener(attach_stream, EventType.STREAM)
    logger.debug("Added listener to attach stream to circuit %s", circuit_id)

    try:
        logger.debug("HTTP HEAD to %s", url)
        response = session.head(url)
    except Exception as e:
        logger.debug(e)
        return 0
    finally:
        controller.remove_event_listener(attach_stream)
    if response and response.ok:
        logger.debug("Response %s", response)
        return int(response.headers["Content-Length"])
    return 0


def is_exit(
    controller: Controller, relay_fp: Optional[str]
) -> Tuple[bool, RouterStatusEntryV3]:
    rs: RouterStatusEntryV3 = controller.get_network_status(relay_fp)
    _is_exit: bool = rs.exit_policy.can_exit_to(port=443)
    logger.debug("Relay %s is exit: %s", relay_fp, _is_exit)
    return _is_exit, rs


def exits_with_bandwidth(consensus: list, bandwidth: int) -> list:
    # consensus = list(consensus)
    helpers: list = list(
        filter(
            lambda r: r.exit_policy
            and r.exit_policy.can_exit_to(
                port=443,
            )
            and Flag.BADEXIT not in r.flags
            and Flag.EXIT in r.flags
            and r.bandwidth >= 2 * bandwidth,
            consensus,
        )
    )
    if helpers:
        return helpers
    helpers = list(
        filter(
            lambda r: r.exit_policy
            and r.exit_policy.can_exit_to(
                port=443,
            )
            and Flag.BADEXIT not in r.flags
            and Flag.EXIT in r.flags
            and r.bandwidth >= bandwidth,
            consensus,
        )
    )
    return helpers


def non_exits_with_bandwidth(consensus: list, bandwidth: int) -> list:
    # consensusl: list = list(consensus)
    helpers: list = list(
        filter(
            lambda r: Flag.EXIT not in r.flags
            and r.bandwidth >= 2 * bandwidth,
            consensus,
        )
    )
    if helpers:
        return helpers
    helpers = list(
        filter(
            lambda r: Flag.EXIT not in r.flags and r.bandwidth >= bandwidth,
            consensus,
        )
    )
    return helpers


def select_helper(
    controller: Controller, relay_fp: str
) -> Tuple[Optional[str], Optional[bool]]:
    logger.debug("Selecting a helper relay.")
    relay_is_exit, rs = is_exit(controller, relay_fp)
    weight = rs.bandwidth
    consensus: list = list(controller.get_network_statuses())
    if not relay_is_exit:
        logger.debug("Relay %s is not an exit, selecting an exit.", relay_fp)
        helpers = exits_with_bandwidth(consensus, weight)
    else:
        logger.debug("Relay %s is an exit, selecting a non-exit.", relay_fp)
        helpers = non_exits_with_bandwidth(consensus, weight)
    if not helpers:
        return None, None
    helper = secrets.choice(helpers)
    logger.info("Selected %s as helper", helper.fingerprint)
    return helper.fingerprint, relay_is_exit


def prepare_measurement(
    controller: Controller,
    session: requests.Session,
    circuit_path: list,
    url: str,
) -> Tuple[int, int]:
    logger.debug("Measuring %s", circuit_path)
    circuit_id = controller.new_circuit(circuit_path, await_build=True)
    logger.debug("Created circuit %s", circuit_id)
    content_length = attach_stream_to_circuit(
        controller, circuit_id, session, url
    )
    return circuit_id, content_length


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "-r",
        "--relay-fp",
        help="The relay (fingerprint) to measure.",
        type=str,
        required=True,
    )
    parser.add_argument(
        "-e",
        "--helper-fp",
        help="A helper relay fingerprint, it should has the same or higher "
        "capacity than the relay to measure. If it is not provided, "
        "a suitable random one will be selected to measure the relay.",
        type=str,
    )
    parser.add_argument(
        "-w",
        "--url",
        help="A destination Web Server URL. To run one see  "
        "https://gitlab.torproject.org/tpo/network-health/bwscanner_webserver",
        type=str,
        required=True,
    )
    parser.add_argument(
        "-u",
        "--upload",
        help="Upload or not (download).",
        action="store_true",
    )
    parser.add_argument(
        "-c",
        "--chunked",
        help="Chunked/stream download/upload or not.",
        action="store_true",
    )
    parser.add_argument(
        "-b",
        "--initial-bytes",
        help="Initial bytes to upload or download",
        type=int,
        default=defaults.INITIAL_BYTES,
    )
    args = parser.parse_args()
    return args


def main() -> None:
    args = parse_args()
    controller: Controller
    proxies: dict
    controller, proxies = launch_tor()
    client = webclient.WebClient(proxies=proxies)
    if not args.helper_fp:
        helper_fp, relay_is_exit = select_helper(controller, args.relay_fp)
        if not helper_fp:
            logger.warning("no helper")
            sys.exit(1)
    else:
        helper_fp = args.helper_fp
        relay_is_exit, _ = is_exit(controller, args.relay_fp)
    if relay_is_exit:
        circuit_path = [helper_fp, args.relay_fp]
        logger.debug("Circuit path %s with relay as exit.", circuit_path)
    else:
        logger.debug("Checking whether helper is an exit.")
        if is_exit(controller, helper_fp)[0]:
            circuit_path = [args.relay_fp, helper_fp]
            logger.debug("Circuit path %s with helper as exit.", circuit_path)
        else:
            logger.error("Either relay or helper must be an exit.")
            sys.exit()
    circuit_id, content_length = prepare_measurement(
        controller, client.session, circuit_path, args.url
    )
    client.do_request(
        args.url,
        initial_bytes=args.initial_bytes,
        content_length=content_length or defaults.CONTENT_LENGTH,
        upload=args.upload,
        chunked=args.chunked,
    )
    controller.close_circuit(circuit_id)
    client.close_session()


if __name__ == "__main__":
    main()
