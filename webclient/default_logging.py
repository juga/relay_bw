LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "custom": {
            "format": "%(asctime)s %(module)s[%(process)s]: [%(levelname)s] "
            + "(%(threadName)s) %(filename)s:%(lineno)s - %(funcName)s - %(message)s",
            "datefmt": "%b %d %H:%M:%S",
        },
    },
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "custom",
        },
        "file": {
            "level": "DEBUG",
            "class": "logging.handlers.TimedRotatingFileHandler",
            "filename": "debug.log",
            "when": "midnight",
            "utc": True,
            "backupCount": 30,
            "formatter": "custom",
        },
    },
    "loggers": {
        # To know when connectionpool calls `_new_conn` or `_make_request`
        "urllib3": {
            "level": "DEBUG",
            "handlers": ["console", "file"],
        },
        "": {
            "level": "INFO",
            "handlers": ["console", "file"],
            "propagate": True,
        },
    },
}
