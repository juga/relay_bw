import logging
from typing import Optional

from . import defaults

logger = logging.getLogger(__name__)


class Measurement:

    def __init__(self, chunk_bytes: int, secs: float = 0):
        self.bytes: int = chunk_bytes
        self.secs: float = secs
        self.bw: float = chunk_bytes / secs if secs else 0

    def __str__(self):
        return f"bytes: {self.bytes}, secs: {self.secs}, bw: {self.bw} B/s"


class MeasurementList:

    def __init__(
        self,
        chunk_bytes: int = defaults.CHUNK_BYTES,
        max_bytes: int = defaults.MAX_BYTES,
        min_bytes: int = defaults.MIN_BYTES,
        target_secs: int = defaults.TARGET_SECS,
        max_secs: int = defaults.MAX_SECS,
        max_attempts: int = defaults.NUM_MEASUREMENTS,
    ):
        self._max_bytes: int = max_bytes
        self._min_bytes: int = min_bytes
        self._max_secs: int = max_secs
        self._target_secs: int = target_secs
        self._max_attempts: int = max_attempts
        self.chunk_bytes: int = chunk_bytes
        self.total_bytes: int = 0
        self.total_secs: float = 0
        self.attempts: int = 0
        self.measurements: list = []

    def __str__(self) -> str:
        measurements_str = "\n".join([str(m) for m in self.measurements])
        return (
            f"chunk bytes: {self.chunk_bytes}, "
            f"total bytes: {self.total_bytes}, "
            f"total secs: {self.total_secs}, "
            f"measurements: \n{measurements_str}"
        )

    @property
    def finish(self) -> bool:
        return (
            self.total_bytes >= self._max_bytes
            or self.total_secs >= self._max_secs
            or self.attempts >= self._max_attempts
        )

    def next_size(self, delta_secs: float) -> int:
        if self.finish:
            logger.debug("Finishing reading chunks")
            return 0
        self.chunk_bytes = min(
            self._max_bytes,
            max(
                self._min_bytes,
                int(self.chunk_bytes / delta_secs * self._target_secs),
            ),
        )
        return self.chunk_bytes

    def add_measurement(
        self, delta_secs: float, chunk_bytes: Optional[int] = None
    ):
        chunk_bytes = chunk_bytes or self.chunk_bytes
        self.measurements.append(
            Measurement(chunk_bytes=chunk_bytes, secs=delta_secs)
        )
        self.total_bytes += chunk_bytes
        self.total_secs += delta_secs
        self.attempts += 1
        # return self.next_size(delta_secs)

    @property
    def results(self) -> list:
        if not self.measurements:
            return []
        max_bw: Measurement = max(self.measurements, key=lambda x: x.bw)
        logger.debug("Measurements: %s", self)
        logger.info("Maximum bandwidth %s KB/s", round(max_bw.bw / 1000))
        return [
            {
                "amount": max_bw.bytes,
                "duration": max_bw.secs,
            }
        ]
