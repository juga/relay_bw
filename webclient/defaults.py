HTTP_TIMEOUT: int = 60
HTTP_HEADERS = {
    "Connection": "keep-alive",
    "User-Agent": "webclient",
    "Accept-Encoding": "identity",
}
HTTP_GET_HEADERS = {
    **HTTP_HEADERS,
    **{
        "Range": "bytes={}-{}",
    },
}
HTTP_POST_HEADERS = {
    **HTTP_HEADERS,
}
MAX_BYTES: int = 1024**3
MIN_BYTES: int = 1
MIN_SECS: int = 5
MAX_SECS: int = 10
TARGET_SECS: int = 6
CHUNK_BYTES: int = 1024**2
# Only for GET
INITIAL_BYTES: int = CHUNK_BYTES * 10
CONTENT_LENGTH = MAX_BYTES
# Only for POST
HTTP_POST_UL_KEY: str = "data"
HTTP_CHUNKED: bool = True
NUM_MEASUREMENTS: int = 5
