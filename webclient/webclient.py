#!/usr/bin/env python
import argparse
import logging
import logging.config
import os
import secrets
import time
from typing import MutableMapping, Optional, Tuple, Union
from urllib3.exceptions import ProtocolError

import requests

from webclient import bytesgen, defaults, measurement

# logging.config.dictConfig(default_logging.LOGGING)
logger = logging.getLogger(__name__)
logger.setLevel(os.environ.get("LOG_LEVEL", "INFO").upper())


def log_headers(msg, headers):
    logger.debug(
        "%s: CL: %s, Range: %s, CR: %s",
        msg,
        headers.get("Content-Length", ""),
        headers.get("Range", ""),
        headers.get("Content-Range", ""),
    )


def range_str(
    initial_bytes=defaults.INITIAL_BYTES,
    content_length=defaults.CONTENT_LENGTH,
):
    start_range = secrets.randbelow(content_length - initial_bytes + 1)
    end_range = start_range + initial_bytes - 1
    length = end_range - start_range
    logger.debug(
        "start_range %s end_range %s length %s", start_range, end_range, length
    )
    range_bytes = f"bytes={start_range}-{end_range}"
    return range_bytes


class WebClient:
    """
    Create requests session that sends timeout and adds headers suitable for
    the bandwidth scanners."""

    def __init__(
        self,
        timeout: int = defaults.HTTP_TIMEOUT,
        verify: bool = True,
        headers: MutableMapping[str, str] = defaults.HTTP_HEADERS,
        proxies: Optional[MutableMapping[str, str]] = None,
    ) -> None:
        logger.debug("Initializing session.")
        self.session = requests.Session()
        self._allow_redirects: bool = False
        self._timeout: int = timeout
        self._verify: Union[str, bool] = (
            verify if isinstance(verify, bool) else verify.lower() == "true"  # type: ignore
        )
        self.headers = headers
        if proxies:
            self.proxies = proxies

    def head(self, url: str, **kwargs) -> Optional[requests.Response]:
        try:
            response = self.session.head(
                url,
                headers=self.headers,
                verify=kwargs.get("verify", self._verify),
                timeout=self._timeout,
                allow_redirects=self._allow_redirects,
                **kwargs,
            )
        except Exception as e:
            logger.debug(e)
            return None
        if not response or not response.ok:
            logger.debug("Not OK HTTP HEAD response: %r", response)
            return None
        log_headers("HTTP HEAD response headers", response.headers)
        return response

    def _get_chunks(
        self, url: str, **kwargs
    ) -> Tuple[Optional[requests.Response], list]:
        # Request for the whole content, then read response by chunks
        initial_bytes = defaults.CONTENT_LENGTH
        logger.debug("Initial bytes to upload %s.", initial_bytes)
        content_length = kwargs.get("content_length", defaults.CONTENT_LENGTH)
        chunk_bytes = kwargs.get("chunk_bytes", defaults.CHUNK_BYTES)
        measurements = measurement.MeasurementList(
            chunk_bytes=chunk_bytes,
            max_bytes=content_length,
            min_bytes=defaults.MIN_BYTES,
            target_secs=defaults.TARGET_SECS,
            max_secs=defaults.MAX_SECS,
        )
        headers = kwargs.get("headers", defaults.HTTP_GET_HEADERS)
        headers["Range"] = range_str(initial_bytes, content_length)
        logger.info("HTTP GET request headers Range: %s", headers["Range"])
        start_time = time.monotonic()
        try:
            response = self.session.get(
                url,
                headers=headers,
                verify=kwargs.get("verify", self._verify),
                timeout=self._timeout,
                allow_redirects=self._allow_redirects,
                stream=True,
            )
        except (
            requests.exceptions.ReadTimeout,
            requests.exceptions.ConnectionError,
        ) as e:
            logger.debug("%s", e)
            return None, []
        if not response or not response.ok:
            logger.debug("Not OK HTTP GET response: %r", response)
            return None, []
        # The `chunk_size` can't be dynamic.
        # If assigned to `None`, it hangs.
        iter_content = response.iter_content(chunk_size=chunk_bytes)
        # Not iterating with for to capture exceptions
        while True:
            try:
                chunk = next(iter_content)
                # logger.debug("Length chunk %s", len(chunk))
            except (
                requests.exceptions.ChunkedEncodingError,
                ProtocolError,
                StopIteration,
            ) as e:
                logger.debug(e)
                break
            # No need to check measurements.finish cause iter_content will
            # raise StpoIteration when there isn't more data
        # Obtain the total time as just one measurement
        end_time = time.monotonic()
        delta_secs = end_time - start_time
        measurements.add_measurement(delta_secs, chunk_bytes=len(chunk))
        log_headers("HTTP GET response headers", response.headers)
        return response, measurements.results

    def _get(
        self, url: str, **kwargs
    ) -> Tuple[Optional[requests.Response], Optional[float]]:
        content_length = kwargs.get("content_length", defaults.CONTENT_LENGTH)
        size_bytes = kwargs.get("size_bytes", defaults.INITIAL_BYTES)

        headers = kwargs.get("headers", defaults.HTTP_GET_HEADERS)
        headers["Range"] = range_str(size_bytes, content_length)
        logger.info("HTTP GET request headers Range: %s", headers["Range"])
        start_time: float = time.monotonic()
        try:
            response = self.session.get(
                url,
                headers=headers,
                verify=kwargs.get("verify", self._verify),
                timeout=self._timeout,
                allow_redirects=self._allow_redirects,
                # stream=True,
            )
        except (
            requests.exceptions.ReadTimeout,
            requests.exceptions.ConnectionError,
            requests.exceptions.ChunkedEncodingError,
        ) as e:
            logger.debug("%s", e)
            return None, None
        end_time: float = time.monotonic()
        if not response or not response.ok:
            logger.debug("Not OK HTTP GET response: %r", response)
            return response, None
        delta_secs: float = end_time - start_time
        log_headers("HTTP GET response headers", response.headers)
        return response, delta_secs

    def _repeat_get(self, url: str, **kwargs):
        size_bytes = kwargs.get("initial_bytes", defaults.INITIAL_BYTES)
        measurements = measurement.MeasurementList(
            size_bytes,
            defaults.MAX_BYTES,
            defaults.MIN_BYTES,
            defaults.TARGET_SECS,
            defaults.MAX_BYTES,
            defaults.NUM_MEASUREMENTS,
        )
        while True:
            response, delta_secs = self._get(url, size_bytes=size_bytes)
            if delta_secs:
                measurements.add_measurement(delta_secs, size_bytes)
                size_bytes = measurements.next_size(delta_secs)
                if not size_bytes:
                    break
            else:
                break
        return response, measurements.results

    def get(
        self, url: str, **kwargs
    ) -> Tuple[Optional[requests.Response], list]:  # type: ignore
        if kwargs.get("chunked", defaults.HTTP_CHUNKED):
            return self._get_chunks(
                url,
                **kwargs,
            )
        return self._repeat_get(url, **kwargs)

    def _post_multipart_chunks(
        self,
        url: str,
        **kwargs,
    ) -> Tuple[Optional[requests.Response], list]:
        response: Optional[requests.Response] = None
        initial_bytes = kwargs.get("initial_bytes", defaults.INITIAL_BYTES)
        # logger.debug("initial bytes to upload %s", initial_bytes)
        content_length = kwargs.get("content_length", defaults.CONTENT_LENGTH)
        chunk_generator = bytesgen.BytesGen(
            chunk_bytes=initial_bytes, max_bytes=content_length
        )
        for chunk in chunk_generator:
            try:
                # `requests` adds the `Content-Type` header
                response = self.session.post(
                    url,
                    headers=self.headers,
                    files={defaults.HTTP_POST_UL_KEY: chunk},
                    verify=kwargs.get("verify", self._verify),
                    timeout=self._timeout,
                    allow_redirects=self._allow_redirects,
                )
            except (
                requests.exceptions.ConnectionError,
                requests.exceptions.ReadTimeout,
            ) as e:
                logger.debug(e)
                break
            if not response.ok:
                break
            chunk_generator.set_delta_secs(response.elapsed.total_seconds())
        # Just the last one
        if response and response.ok:
            log_headers("HTTP POST response headers", response.headers)
            logger.info("HTTP POST JSON response %s", response.json())
        return response, chunk_generator.results

    def _post(
        self, url: str, **kwargs
    ) -> Tuple[Optional[requests.Response], Optional[float]]:
        size_bytes: int = kwargs.get("size_bytes", defaults.INITIAL_BYTES)
        chunk = secrets.token_bytes(size_bytes)
        try:
            response = self.session.post(
                url,
                headers=defaults.HTTP_POST_HEADERS,
                files={defaults.HTTP_POST_UL_KEY: chunk},
                verify=kwargs.get("verify", self._verify),
                timeout=self._timeout,
                allow_redirects=self._allow_redirects,
            )
        except (
            requests.exceptions.ConnectionError,
            requests.exceptions.ReadTimeout,
        ) as e:
            logger.debug(e)
            return None, None
        if not response.ok:
            return response, None
        # end_time = time.monotonic()
        log_headers("HTTP POST response headers", response.headers)
        logger.info("HTTP POST JSON response %s", response.json())
        return response, response.elapsed.total_seconds()

    def _repeat_post(self, url: str, **kwargs):
        size_bytes = kwargs.get("initial_bytes", defaults.INITIAL_BYTES)
        measurements = measurement.MeasurementList(
            size_bytes,
            defaults.MAX_BYTES,
            defaults.MIN_BYTES,
            defaults.TARGET_SECS,
            defaults.MAX_BYTES,
            defaults.NUM_MEASUREMENTS,
        )
        while True:
            response, delta_secs = self._post(url, size_bytes=size_bytes)
            if delta_secs:
                measurements.add_measurement(delta_secs, size_bytes)
                size_bytes = measurements.next_size(delta_secs)
                if not size_bytes:
                    break
            else:
                break
        return response, measurements.results

    def post(
        self, url: str, **kwargs
    ) -> Tuple[Optional[requests.Response], list]:  # type: ignore
        if kwargs.get("chunked", defaults.HTTP_CHUNKED):
            return self._post_multipart_chunks(url, **kwargs)
        return self._repeat_post(url, **kwargs)

    def close_session(self) -> None:
        self.session.close()

    def do_request(
        self,
        url: str,
        verify: bool = True,
        initial_bytes: int = defaults.INITIAL_BYTES,
        content_length: int = defaults.CONTENT_LENGTH,
        upload: bool = False,
        chunked: bool = defaults.HTTP_CHUNKED,
    ) -> Optional[list]:
        # Start with HEAD request to create the connection
        self.head(url)  # , verify=verify)
        if upload:
            response, results = self.post(
                url,
                verify=verify,
                initial_bytes=initial_bytes,
                chunked=chunked,
            )
        else:
            response, results = self.get(
                url,
                verify=verify,
                initial_bytes=initial_bytes,
                content_length=content_length,
                chunked=chunked,
            )
        return results


def parse_args() -> argparse.Namespace:
    parser: argparse.ArgumentParser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "-w",
        "--url",
        help="A destination Web Server URL. ",
        type=str,
        default="http://localhost:4443",
    )
    parser.add_argument(
        "-u",
        "--upload",
        help="Upload or not (download).",
        action="store_true",
    )
    parser.add_argument(
        "-c",
        "--chunked",
        help="Chunked/stream download/upload or not.",
        action="store_true",
    )
    parser.add_argument(
        "-b",
        "--initial-bytes",
        help="Initial bytes to upload or download",
        type=int,
        default=defaults.INITIAL_BYTES,
    )
    args: argparse.Namespace = parser.parse_args()
    return args


def main() -> None:
    args: argparse.Namespace = parse_args()
    webclient: WebClient = WebClient()
    webclient.do_request(
        args.url,
        upload=args.upload,
        initial_bytes=args.initial_bytes,
        chunked=args.chunked,
    )
    webclient.close_session()


if __name__ == "__main__":
    main()
